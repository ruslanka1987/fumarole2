<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Покупки");
?> 
  <h4>Оформление заказа</h4>
 	 
  <p style="text-align: justify;"><img width="200" src="<?=SITE_DIR?>buys/Oformlenie-zakaza-300x197.jpg" height="131" hspace="5" border="0" align="left" vspace="5"></p><p style="text-align: justify;"><b style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px;">УВАЖАЕМЫЕ ПОКУПАТЕЛИ !</b></p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;"><b>Для совершения покупки в интернет-магазине</b>&nbsp;вам необходимо:</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">1.&nbsp;Зарегистрироваться в интернет-магазине. Если вы не хотитите регистрироваться, то данный пункт вы можете пропустить и переходить к п.2.</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">2. Поместить интересующий вас товар в «корзину» и оформить заказ.</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">3. Выбрать тип доставки:</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">- самовывоз из магазина;</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">- доставка по г.Челябинск (стоимость доставки см. ниже);</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">- доставка транспортной компанией в другие города.</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">4.Выбрать вид оплаты:</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">- наличные (оплата в магазине при получении товара или оплата курьеру по г.Челябинск);</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">- оплата&nbsp;банковской картой (принимаются карты Visa, MasterCard);</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">- счет для оплаты (оператор высылает Вам на электронный адрес&nbsp;счет на оплату с&nbsp;доставкой, который Вы можете оплатить&nbsp;на почте, в Сбербанке и т.д.).&nbsp;</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">5. Ваш заказ оформлен. Ждите ответа оператора&nbsp;по электронной почте.</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">При оплате товара банковской картой&nbsp;Вы автоматически &nbsp;перенаправляетесь на сайт системы обработки платежей. Далее нажимаете кнопку "Перейти" и заполняете приводимую ниже форму, после чего нажимаете "Оплатить". Предварительно, уточните наличие товара на складе у опретора. При получении товара методом самовывоза - необходимо личное присутствие держателя карты. При себе иметь документ, удостоверяющий личность. Если Вы заказываете товар с доставкой, то&nbsp;сумму доставки надо уточнить у оператора.&nbsp;</p>

  <p style="color: #333333; font-family: Tahoma, Verdana, Helvetica, sans-serif; font-size: 13px; line-height: normal; background-color: #ffffff;">При выборе платежной системы "Счет на оплату" с Вами связывается оператор и высылает&nbsp;на указанный электронный адрес&nbsp;счет на оплату&nbsp;с учетом доставки товара до вашего города. Товар&nbsp;резервируется за Вами сроком до 3-х дней с момента выставления счета. После поступления оплаты на наш р/с, Вам отсылается товар. Во избежание недоразумений подтверждайте свою оплату: либо по телефону, либо по электронке.</p>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
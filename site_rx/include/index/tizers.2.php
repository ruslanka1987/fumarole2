<div>
	<div class="tiles_bgn_wrapper">
		<div class="uni-indents-vertical indent-15"></div>
			<div class="header-main">
				<div class="text">Наши преимущества</div>
			</div>
			<div class="uni-indents-vertical indent-20"></div>
			<div>
				<?$APPLICATION->includeComponent("bitrix:main.include", "", array(
					"AREA_FILE_SHOW" => "file",
					"PATH" => SITE_DIR."include/index/tizers.2.description.php"
				), false)?>
			</div>
			<div class="uni-indents-vertical indent-40"></div>
			<?$APPLICATION->IncludeComponent(
				"intec:custom.iblock.element.list", 
				"tizers.2", 
				array(
					"COMPONENT_TEMPLATE" => "tizers.2",
					"IBLOCK_ELEMENTS_ID" => array(
					),
					"PICTURE_WIDTH" => "60",
					"USE_DETAIL_PICTURE" => "Y",
					"USE_PREVIEW_PICTURE" => "Y",
					"PICTURE_HEIGHT" => "60",
					"NO_PICTURE_PATH" => SITE_TEMPLATE_PATH."/images/noimg/no-img.png",
					"IBLOCK_SECTION" => "",
					"IBLOCK_TYPE" => "content",
					"IBLOCK_ID" => "25",
					"IBLOCK_SECTION_CODE" => "on_main",
					"IBLOCK_ELEMENTS_COUNT" => "6",
					"IBLOCK_SORT_FIELD" => "ID",
					"IBLOCK_SORT_ORDER" => "asc"
				),
				false
			);?>
		<div class="uni-indents-vertical indent-40"></div>
	</div>
</div>
<div class="uni-indents-vertical indent-40"></div>
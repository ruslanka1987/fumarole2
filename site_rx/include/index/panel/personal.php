<div class="panel-item right">
	<div class="panel-item-wrap">
		<?$APPLICATION->IncludeComponent(
			"bitrix:system.auth.form", 
			".default", 
			array(
				"COMPONENT_TEMPLATE" => ".default",
				"REGISTER_URL" => SITE_DIR."personal/",
				"FORGOT_PASSWORD_URL" => SITE_DIR."personal/",
				"PROFILE_URL" => SITE_DIR."personal/",
				"SHOW_ERRORS" => "N"
			),
			false
		);?>
	</div>
</div>
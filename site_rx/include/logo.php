<a href="<?=SITE_DIR?>">
    <svg version="1.1" style="max-width: 147px; max-height: 60px; width: 100%;" id="layer1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    	 viewBox="0 0 157.7 64.2" enable-background="new 0 0 157.7 64.2" xml:space="preserve">
    <defs>
        <style type="text/css">
            .fil0 {fill:#353535}
            .fil2 {fill:#FCFCFC}
            .fil3 {fill:white}
        </style>
    </defs>
    <path class="svg_fill_solid" d="M116.3,22.4l12.8-7.4c3.3-1.9,7.3-1.9,10.5,0l12.8,7.4c3.3,1.9,5.3,5.4,5.3,9.1v14.8c0,3.8-2,7.2-5.3,9.1
    	l-12.8,7.4c-3.3,1.9-7.3,1.9-10.5,0l-12.8-7.4c-3.3-1.9-5.3-5.4-5.3-9.1V31.5C111.1,27.7,113.1,24.3,116.3,22.4z"/>
    <text transform="matrix(1 0 0 1 120.0926 44.0582)" class="fil3" font-family="'Ubuntu'" font-size="17">site</text>
    <g>
    	<g>
    		<path class="svg_fill_solid" d="M0,42.8V17.2h12.8v25.4c0,6.6,3.3,9.7,8.4,9.7s8.4-3,8.4-9.4V17.2h12.8v25.3c0,14.7-8.4,21.2-21.4,21.2
    			C8.2,63.7,0,57.1,0,42.8z"/>
    		<g>
    			<polygon class="svg_fill_solid" points="73.4,17.9 73.4,17.2 73.4,17.2 54.6,17.2 54.6,17.2 73.4,41.3 			"/>
    			<g>
    				<polygon class="svg_fill_solid" points="74.8,62.8 74.8,62.8 55.3,37.8 55.3,62.8 74.8,62.9 				"/>
    				<polygon class="svg_fill_solid" points="85.9,17.2 85.9,17.2 85.9,17.9 85.9,60.7 85.9,62.8 85.9,62.9 98.6,62.9 98.6,17.2 				"/>
    			</g>
    		</g>
    		<path class="svg_fill_solid" d="M76.7,11l-1.8-1.8c-1.6-1.6-1.6-4.2,0-5.8l1.8-1.8c1.6-1.6,4.2-1.6,5.8,0l1.8,1.8c1.6,1.6,1.6,4.2,0,5.8
    			L82.5,11C80.9,12.6,78.3,12.6,76.7,11z"/>
    	</g>
    </g>
    </svg>
</a>
<div class="header-main">
    <div class="text">Контакты</div>
</div>
<div class="uni-indents-vertical indent-20"></div>
<?$APPLICATION->IncludeComponent(
	"intec:custom.iblock.element.list", 
	"contacts.landing.1", 
	array(
		"COMPONENT_TEMPLATE" => "contacts.landing.1",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "8",
		"IBLOCK_ELEMENTS_ID" => array(
			0 => "79",
			1 => "80",
			2 => "81",
		),
		"PICTURE_WIDTH" => "600",
		"USE_DETAIL_PICTURE" => "N",
		"USE_PREVIEW_PICTURE" => "N",
		"PICTURE_HEIGHT" => "600",
		"NO_PICTURE_PATH" => SITE_TEMPLATE_PATH."/images/noimg/no-img.png",
		"IBLOCK_SECTION" => "",
		"IBLOCK_SECTION_CODE" => "",
		"IBLOCK_ELEMENTS_COUNT" => "0",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"IBLOCK_SORT_FIELD" => ""
	),
	false
);?>
<div class="panel-item">
    <div class="panel-item-wrap">
        <div class="panel-item-icon" style="background-image: url('<?=SITE_TEMPLATE_PATH?>/images/icons/panel.top.email.png');"></div>
        <div class="uni-aligner-vertical"></div>
        <div class="panel-item-content">
            <?$APPLICATION->includeComponent("bitrix:main.include", "", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => SITE_DIR."include/company_email.php"
            ), false)?>
        </div>
    </div>
</div>
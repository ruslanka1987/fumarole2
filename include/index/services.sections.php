<div class="tiles_bgn tiles_bgn-white" id="<?=$thisID?>">
    <div class="tiles_bgn_wrapper">
        <div class="uni-indents-vertical indent-25"></div>
        <div class="header-main">
            <a href="/product/" class="text">Наша продукция</a>
        </div>
        <div class="uni-indents-vertical indent-40"></div>
        <?$APPLICATION->IncludeComponent(
            "bitrix:catalog.section.list",
            "sections.2",
            array(
                "COMPONENT_TEMPLATE" => "sections.2",
                "IBLOCK_TYPE" => "catalog",
                "IBLOCK_ID" => "20",
                "SECTION_ID" => "",
                "SECTION_CODE" => "",
                "COUNT_ELEMENTS" => "Y",
                "TOP_DEPTH" => "1",
                "SECTION_FIELDS" => array(
                    0 => "",
                    1 => "",
                ),
                "SECTION_USER_FIELDS" => array(
                    0 => "UF_SEO_TEXT",
                    1 => "",
                ),
                "SECTION_URL" => "",
                "CACHE_TYPE" => "N",
                "CACHE_TIME" => "36000000",
                "CACHE_GROUPS" => "Y",
                "ADD_SECTIONS_CHAIN" => "N",
                "SECTIONS" => array(
                    0 => "",
                    1 => "",
                ),
                "ELEMENTS_COUNT" => "6",
                "FILTER_NAME" => "sectionsFilter",
                "CACHE_FILTER" => "N"
            ),
            false
        );?>
    </div>
</div>
<div class="tiles_bgn_size" id="<?=$thisID2?>"></div>
<div class="uni-indents-vertical indent-40"></div>
<!--end tiles_bgn tiles_bgn-white-->

<?$thisID  = 'services_bgn';?>
<?$thisID2 = 'services_bgn_size';?>
<div class="tiles_bgn  tiles_bgn-has-carousel" id="<?=$thisID?>">
	<div class="tiles_bgn_wrapper">
		<div class="uni-indents-vertical indent-25"></div>
		<div class="header-main">
			<div class="text">Наши услуги</div>
		</div>
		<div class="uni-indents-vertical indent-40"></div>
		<?$APPLICATION->IncludeComponent(
	"intec:custom.iblock.element.list",
	"tiles.landing.1",
	array(
		"COMPONENT_TEMPLATE" => "tiles.landing.1",
		"IBLOCK_TYPE" => "catalog",
		"IBLOCK_ID" => "28",
		"IBLOCK_ELEMENTS_ID" => array(
		),
		"PICTURE_WIDTH" => "600",
		"USE_DETAIL_PICTURE" => "N",
		"USE_PREVIEW_PICTURE" => "Y",
		"PICTURE_HEIGHT" => "600",
		"NO_PICTURE_PATH" => SITE_TEMPLATE_PATH."/images/noimg/no-img.png",
		"IBLOCK_SECTION" => "",
		"USE_LINK_TO_ELEMENTS" => "Y",
		"LINK_TO_ELEMENTS" => "",
		"PICTURE_BLOCK_HEIGHT" => "70%",
		"IBLOCK_SECTION_CODE" => "",
		"IBLOCK_ELEMENTS_COUNT" => "4",
		"IBLOCK_SORT_FIELD" => "SORT",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO",
		"IBLOCK_SORT_ORDER" => "asc"
	),
	false
);?>
	</div>
</div>
<div class="tiles_bgn_size" id="<?=$thisID2?>"></div>
<script>
	$tilesHeight<?=$thisID?> = $('#<?=$thisID?>').outerHeight(false);
	$('#<?=$thisID2?>').css('height', $tilesHeight<?=$thisID?>);

	$(window).resize(function() {
		$tilesHeight<?=$thisID?> = $('#<?=$thisID?>').outerHeight(false);
		$('#<?=$thisID2?>').css('height', $tilesHeight<?=$thisID?>);
	});
</script>

<div class="uni-indents-vertical indent-40"></div>

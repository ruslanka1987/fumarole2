<div class="header-main">
    <div class="text">Как мы работаем</div>
</div>
<div class="uni-indents-vertical indent-40"></div>
<?$APPLICATION->IncludeComponent(
	"intec:custom.iblock.element.list", 
	"tizers.landing.1", 
	array(
		"COMPONENT_TEMPLATE" => "tizers.landing.1",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "25",
		"IBLOCK_ELEMENTS_ID" => array(
		),
		"PICTURE_WIDTH" => "350",
		"USE_DETAIL_PICTURE" => "Y",
		"USE_PREVIEW_PICTURE" => "Y",
		"PICTURE_HEIGHT" => "350",
		"NO_PICTURE_PATH" => SITE_TEMPLATE_PATH."/images/noimg/no-img.png",
		"IBLOCK_SECTION" => "",
		"IBLOCK_SECTION_CODE" => "how_we_work",
		"IBLOCK_ELEMENTS_COUNT" => "5",
		"IBLOCK_SORT_FIELD" => ""
	),
	false
);?>
<div class="uni-indents-vertical indent-40"></div>
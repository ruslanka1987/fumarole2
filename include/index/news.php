<?$thisID  = 'news_bgn';?>
<?$thisID2 = 'news_bgn_size';?>
<div class="tiles_bgn" id="<?=$thisID?>">
    <div class="tiles_bgn_wrapper">
        <div class="uni-indents-vertical indent-15"></div>
        <div class="header-main">
            <a href="/company/news/" class="text">Новости</a>
        </div>
        <div class="uni-indents-vertical indent-20"></div>
        <?$APPLICATION->IncludeComponent("bitrix:news.list", "news.horizontal.1", Array(
            "IBLOCK_TYPE" => "content",
            "IBLOCK_ID" => "11",
            "NEWS_COUNT" => "4",
            "SORT_BY1" => "ACTIVE_FROM",
            "SORT_ORDER1" => "DESC",
            "SORT_BY2" => "SORT",
            "SORT_ORDER2" => "ASC",
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "36000000",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "N",
            "PREVIEW_TRUNCATE_LEN" => "110",
            "ACTIVE_DATE_FORMAT" => "j M Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
            "ADD_SECTIONS_CHAIN" => "N",
            "DISPLAY_TOP_PAGER" => "N",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "AJAX_OPTION_ADDITIONAL" => "",
        ),
            false
        );?>
        <div class="uni-indents-vertical indent-30"></div>
    </div>
</div>
<div class="tiles_bgn_size" id="<?=$thisID2?>"></div>
<script>
    $tilesHeight<?=$thisID?> = $('#<?=$thisID?>').outerHeight(false);
    $('#<?=$thisID2?>').css('height', $tilesHeight<?=$thisID?>);

    $(window).resize(function() {
        $tilesHeight<?=$thisID?> = $('#<?=$thisID?>').outerHeight(false);
        $('#<?=$thisID2?>').css('height', $tilesHeight<?=$thisID?>);
    });
</script>
<div class="uni-indents-vertical indent-40"></div>

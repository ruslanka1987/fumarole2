<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>    					<?if(!$bIsFrontPage){?>	
    						<?if($bShowMenuLeft){?>
    							</div>
    						<?}?>
    					<?}?>
    					<div class="clear"></div>
    				</div>
    			</div>
    		</div>
    		<div class="clear"></div>
		</div>
		<div class="bg_footer">
			<div class="footer">
				<div class="contacts left">

                    <div class="uni-indents-vertical indent-25"></div><img src='/images/cp.svg' height='40' />
                    <div class="copyright">
                        © 2015 &mdash; <?=date('Y')?> ТД Реакция<br>Все права защищены
                    </div>
                    <div class="uni-text-default">
    					<?$APPLICATION->IncludeFile(SITE_DIR."include/footer_contacts.php", array(), array(
    						"MODE" => "html",
    						"NAME" => ""
    					));?>
                    </div>
				</div>
				<div class="menu left">
                    <div class="uni-indents-vertical indent-25"></div>
    					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottom_menu", array(
    						"ROOT_MENU_TYPE" => "bottom",
    						"MENU_CACHE_TYPE" => "A",
    						"MENU_CACHE_TIME" => "600000",
    						"MENU_CACHE_USE_GROUPS" => "N",
    						"MENU_CACHE_GET_VARS" => array(
    						),
							"CACHE_SELECTED_ITEMS" => "N", // Не создавать кеш меню для каждой страницы
    						"MAX_LEVEL" => "2",
    						"CHILD_MENU_TYPE" => "bottom_left",
    						"USE_EXT" => "Y",
    						"DELAY" => "N",
    						"ALLOW_MULTI_SELECT" => "N"
    						), false
    					);?>
					<div class="clear"></div>
				</div>
                <div class="phone-block right">
                    <div class="uni-indents-vertical indent-25"></div>
                    <div class="phone-block">
                        <div class="phone">
							<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
									"AREA_FILE_SHOW" => "file", 
									"PATH" => SITE_DIR."include/company_phone.php", 
								)
							);?>
						</div>
                    </div>
                    <div class="logo-block">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
    							"AREA_FILE_SHOW" => "file", 
    							"PATH" => SITE_DIR."include/company_logo.php", 
    						)
    					);?>
                    </div>
                </div>
                <div class="clear"></div>
                <div class="uni-indents-vertical indent-25"></div>
				<div id="bx-composite-banner"></div>
			</div>
		</div>
    	<?if ($options['SHOW_BUTTON_TOP']['ACTIVE_VALUE'] == 'Y'):?>
    		<div class="button_up solid_button">
    			<i></i>
    		</div>
    	<?endif;?>
    	<script>
    		$('.nbs-flexisel-nav-left').addClass('uni-slider-button-small').addClass('uni-slider-button-left').html('<div class="icon"></div>');
    		$('.nbs-flexisel-nav-right').addClass('uni-slider-button-small').addClass('uni-slider-button-right').html('<div class="icon"></div>');
    	</script>
    </body>
</html>
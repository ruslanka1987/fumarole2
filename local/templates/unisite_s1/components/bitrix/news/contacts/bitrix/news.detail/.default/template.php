<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="contact">
	<div class="section section-left">
		<div itemscope itemtype="http://schema.org/Organization">
		<?if (!empty($arResult['PROPERTIES']['ADDRESSLOCALITY']['VALUE']) || !empty($arResult['DISPLAY_PROPERTIES']['STREETADDRESS']['VALUE'])):?>
			<div class="field" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
				<div class="title"><?=GetMessage('CONTACT_ADDRESS')?>:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<div class="text">
					<span itemprop="postalCode"><?=$arResult['PROPERTIES']['POSTALCODE']['VALUE']?></span>
					<span itemprop="addressLocality"><?=$arResult['PROPERTIES']['ADDRESSLOCALITY']['VALUE']?></span>
					<span itemprop="streetAddress"><?=$arResult['PROPERTIES']['STREETADDRESS']['VALUE']?></span>
				</div>
			</div>
			<div class="uni-indents-vertical indent-50"></div>
		<?endif;?>
		<?if (!empty($arResult['DISPLAY_PROPERTIES']['PHONES']['VALUE'])):?>
			<div class="field">
				<div class="title"><?=GetMessage('CONTACT_PHONE')?>:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<?foreach ($arResult['DISPLAY_PROPERTIES']['PHONES']['VALUE'] as $arPhone):?>
					<div class="text" itemprop="telephone"><?=$arPhone?></div>
				<?endforeach;?>
			</div>
			<div class="uni-indents-vertical indent-50"></div>
		<?endif;?>
		<?if (!empty($arResult['DISPLAY_PROPERTIES']['WORK']['VALUE'])):?>
			<div class="field">
				<div class="title"><?=GetMessage('CONTACT_WORK')?>:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<div class="text"><?=$arResult['DISPLAY_PROPERTIES']['WORK']['VALUE']?></div>
			</div>
			<div class="uni-indents-vertical indent-50"></div>
		<?endif;?>
		<?if (!empty($arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE'])):?>
			<div class="field">
				<div class="title"><?=GetMessage('CONTACT_EMAIL')?>:</div>
				<div class="uni-indents-vertical indent-10"></div>
				<div class="text" itemprop="email"><?=$arResult['DISPLAY_PROPERTIES']['EMAIL']['VALUE']?></div>
			</div>
		<?endif;?>
		</div>
	</div>
	<div class="section section-right">
		<?if (!empty($arResult['PREVIEW_TEXT'])):?>
			<div class="description">
				<?=$arResult['PREVIEW_TEXT']?>
			</div>
		<div class="clear"></div>
		<div class="uni-indents-vertical indent-20"></div>
		<?endif;?>
		<?if (!empty($arResult['DISPLAY_PROPERTIES']['MAP']['VALUE'])):?>
			<div class="map">
				<?=$arResult['DISPLAY_PROPERTIES']['MAP']['DISPLAY_VALUE']?>
			</div>
			<div class="clear"></div>
		<?endif;?>
		<?if (!empty($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'])):?>
			<div class="images uni_parent_col">
				<?foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $pictureId):?>
					<?
						$pictureFile = CFile::ResizeImageGet($pictureId, array('width' => 500, 'height' => 500), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
						
					if ($pictureFile):?>
					<a rel="images_<?=$arResult['ID']?>" class="image uni_col uni-25 fancy" href="<?=$pictureFile['src']?>">
						<img src="<?=$pictureFile['src']?>" />
					</a>
					<?endif;?>
				<?endforeach;?>
			</div>
			<div class="clear"></div>
		<?endif;?>
	</div>
	<div class="clear"></div>
</div>
<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();?>
<?
    if (CModule::IncludeModule('intec.startshop'))
	{
	   $arItems = array();
       
	   foreach ($arResult['ITEMS'] as $iKey => $arItem)
           $arItems[] = $arItem['ID'];
           
       if (!empty($arItems))
       {
           $arCatalogItems = CStartShopCatalogProduct::GetList(array(), array(
               'ID' => $arItems
           ));
           
           $arItems = array();
           
           foreach ($arCatalogItems as $arCatalogItem)
               $arItems[$arCatalogItem['ID']] = $arCatalogItem;
               
           foreach ($arResult['ITEMS'] as $iKey => $arItem)
           {
               $arResult['ITEMS'][$iKey]['PRICES'] = $arItems[$arItem['ID']]['STARTSHOP']['PRICES'];
               $arResult['ITEMS'][$iKey]['MIN_PRICE'] = $arItems[$arItem['ID']]['STARTSHOP']['MIN_PRICE'];
           }
               
       }    
       
    }
?>
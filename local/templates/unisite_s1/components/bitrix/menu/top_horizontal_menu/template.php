<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true)?>
<?$li_width = 100/count($arResult);?>
<?
if (empty($arResult))
	return;
?>
<div class="bg_top_menu<?=$arParams["TYPE_MENU"] == 'solid'?' solid solid_element':' '.$arParams["TYPE_MENU"]?><?=$arParams["MENU_WIDTH_SIZE"] == "Y"?' wide':' normal'?><?=' '.$arParams["MENU_IN"]?>">
	<div class="radius_top_menu">
		<div id="min_menu_mobile" class="min_menu solid_element"><?=GetMessage("MENU_TITLE")?></div>
		<table class="top_menu" cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				<td class="td_delimiter">
					<hr>
				</td>
				<?foreach( $arResult as $key => $arItem ){?>
					<? 
						$isCatalog = false;
                        $isServices = false;
                        
    					if($arItem["LINK"] == $arParams["IBLOCK_CATALOG_DIR"] && !empty($arParams["IBLOCK_CATALOG_TYPE"]) && !empty($arParams["IBLOCK_CATALOG_ID"])) {
                            $sIBlockType = $arParams["IBLOCK_CATALOG_TYPE"];
                            $sIBlockID = $arParams["IBLOCK_CATALOG_ID"];
                            $isCatalog = true;
    					}
    					
                        if ($arItem["LINK"] == $arParams['IBLOCK_SERVICES_DIR'] && !empty($arParams["IBLOCK_SERVICES_TYPE"]) && !empty($arParams["IBLOCK_SERVICES_ID"])) {
                            $isServices = true;
                            $sIBlockType = $arParams["IBLOCK_SERVICES_TYPE"];
                            $sIBlockID = $arParams["IBLOCK_SERVICES_ID"];
                        }
						
					if(!$isCatalog && $arParams["TYPE_MENU"] == "catalog" || $arParams["TYPE_MENU"] != "catalog") {?>
						<td <?if ($arParams['SMOOTH_COLUMNS'] == 'Y'):?>style="width:<?=$li_width?>%"<?endif;?> class="<?=$arItem['SELECTED']?'current':''?> <?=$isCatalog?'td_catalog parent':''?> <?=$isServices?'td_services parent':''?> <?=$arItem["IS_PARENT"]?'parent':''?>">
							<a href="<?=$arItem["LINK"]?>" class="title_f ">
								<span class="arrow">
									<?=$arItem["TEXT"]?>
								</span>
							</a>
							<a href="<?=$arItem["LINK"]?>" class="mobile_link">
								<span class="arrow">
									<?=$arItem["TEXT"]?>
								</span>
							</a>
							<?if($isCatalog){?>
								<?$APPLICATION->IncludeComponent(
									"bitrix:catalog.section.list",
									"top_catalog",
									Array(
										"IBLOCK_TYPE" => $arParams["IBLOCK_CATALOG_TYPE"],
										"IBLOCK_ID" => $arParams["IBLOCK_CATALOG_ID"],
										"SECTION_ID" => "",
										"SECTION_CODE" => "",
										"COUNT_ELEMENTS" => "N",
										"TOP_DEPTH" => "2",
										"SECTION_FIELDS" => array(0=>"",1=>"",),
										"SECTION_USER_FIELDS" => array(0=>"",1=>"",),
										"SECTION_URL" => "",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_GROUPS" => "Y",
										"ADD_SECTIONS_CHAIN" => "N"
									),
									$component,
									array('HIDE_ICONS'=>'Y')
								);?>
							<?}?>
							 <?if($isServices){?>
                                <?
                                    $arSubItems = array();
                                    $arSections = $APPLICATION->IncludeComponent('bitrix:menu.sections', '', array(
                                        "IBLOCK_TYPE" => $sIBlockType,
										"IBLOCK_ID" => $sIBlockID,
                                        "SECTION_ID" => "",
										"SECTION_CODE" => "",
                                        "TOP_DEPTH" => "1",
                                        "SECTION_URL" => "",
										"CACHE_TYPE" => "A",
										"CACHE_TIME" => "36000000",
										"CACHE_GROUPS" => "Y",
										"ADD_SECTIONS_CHAIN" => "N"
                                    ));
                                    
                                    foreach ($arSections as $arSection) {
                                        $arSection['3']['DEPTH_LEVEL']++;
                                        
                                        $arSubItem = array();
                                        $arSubItem['TEXT'] = $arSection[0];
                                        $arSubItem['LINK'] = $arSection[1];
                                        $arSubItem['SELECTED'] = false;
                                        $arSubItem['ITEM_TYPE'] = 'D';
                                        $arSubItem['DEPTH_LEVEL'] = $arSection['3']['DEPTH_LEVEL'];
                                        $arSubItem['IS_PARENT'] = $arSection['3']['IS_PARENT'];
                                        $arSubItems[] = $arSubItem;
                                    }
                                    
                                    if (!empty($arSubItems)) {
                                        $arItem['IS_PARENT'] = 1;
                                        $arItem['ITEMS'] = $arSubItems;
                                    }
                                ?>
                            <?}?>
							<?if( $arItem["IS_PARENT"] ){?>
								<div class="child submenu">						
									<?foreach( $arItem["ITEMS"] as $arSubItem ){?>
										<?if($arSubItem["DEPTH_LEVEL"]>2) {
											continue;
										}?>
										<a class="hover_link" href="<?=$arSubItem["LINK"]?>"><?=$arSubItem["TEXT"]?></a>
									<?}?>
								</div>
								<div class="submenu_mobile">						
									<?foreach( $arItem["ITEMS"] as $arSubItem ){?>
										<?if($arSubItem["DEPTH_LEVEL"]>2) {
											continue;
										}?>
										<a class="hover_link" href="<?=$arSubItem["LINK"]?>"><?=$arSubItem["TEXT"]?></a>
									<?}?>
								</div>
							<?}?>					
							
						</td>
					<?}?>
				<?}?>
			<td>
			<div class="search_wrap">
					<?$APPLICATION->IncludeComponent(
						"bitrix:search.title", 
						"header_search", 
						array(
							"NUM_CATEGORIES" => "1",
							"TOP_COUNT" => "5",
							"ORDER" => "date",
							"USE_LANGUAGE_GUESS" => "Y",
							"CHECK_DATES" => "N",
							"SHOW_OTHERS" => "N",
							"PAGE" => SITE_DIR."catalog/",
							"CATEGORY_0_TITLE" => GetMessage("SEARCH_GOODS"),
							"CATEGORY_0" => array(
							),
							"CATEGORY_0_iblock_catalog" => array(
								0 => "all",
							),
							"SHOW_INPUT" => "Y",
							"INPUT_ID" => "title-search-input",
							"CONTAINER_ID" => "search",
							"PRICE_CODE" => array(
								0 => "BASE",
							),
							"PRICE_VAT_INCLUDE" => "Y",
							"PREVIEW_TRUNCATE_LEN" => "",
							"SHOW_PREVIEW" => "Y",
							"PREVIEW_WIDTH" => "300",
							"PREVIEW_HEIGHT" => "300",
							"CONVERT_CURRENCY" => "Y",
							"CURRENCY_ID" => "RUB",
							"COMPONENT_TEMPLATE" => "header_search"
						),
						false
					);?>
				</div>
			</td>
			</tr>
		</table>
	</div>
</div>



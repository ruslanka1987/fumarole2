<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?$this->setFrameMode(true);?>
<div class="auth-form-default">
    <?if($arResult["FORM_TYPE"] == "login"):?>
        <div class="uni-aligner-vertical"></div>
        <div class="auth-form-default-icon"></div>
        <a class="auth-form-default-button auth-form-default-login" onclick="return openAuthorizePopup();" title="<?=GetMessage("AUTH_FORM_DEFAULT_LOGIN")?>">
            <?=GetMessage("AUTH_FORM_DEFAULT_LOGIN")?>
        </a>
        <script>
            function openAuthorizePopup() {
                if(window.innerWidth < 790) {
                    document.location.href = "<?=$arResult["AUTH_URL"]?>";
                }else{
                    var authPopup = BX.PopupWindowManager.create("AuthorizePopup", null, {
                        autoHide: true,
                        offsetLeft: 0,
                        offsetTop: 0,
                        overlay : true,
                        draggable: {restrict:true},
                        closeByEsc: true,
                        closeIcon: { right : "32px", top : "23px"},
                        content: '<div style="width:702px; height:303px; text-align: center;"><span style="position:absolute;left:50%; top:50%"></span></div>',
                        events: {
                            onAfterPopupShow: function() {
                                BX.ajax.post(
                                    '<?=$this->GetFolder().'/ajax/authorize.php'?>',
                                    {
                                        backurl: '<?=htmlspecialchars_decode($arResult["BACKURL"])?>',
                                        sAuthUrl: '<?=htmlspecialchars_decode($arResult["AUTH_URL"])?>',
                                        sForgotPasswordUrl: '<?=htmlspecialchars_decode($arResult["AUTH_FORGOT_PASSWORD_URL"])?>',
                                        sRegisterUrl: '<?=htmlspecialchars_decode($arResult["AUTH_REGISTER_URL"])?>',
                                        SITE_ID: '<?=SITE_ID?>'
                                    },
                                    BX.delegate(function(result)
                                        {
                                            this.setContent(result);
                                        },
                                        this)
                                );
                            }
                        }
                    });
                    authPopup.show();
                }
            }
        </script>
    <?else:?>
        <form action="<?=$arResult["AUTH_URL"]?>" method="POST" style="height: 100%;">
            <?$frame = $this->createFrame()->begin();?>
                <div class="uni-aligner-vertical"></div>
                <div class="auth-form-default-icon"></div>
                <a class="auth-form-default-button auth-form-default-profile" href="<?=$arResult["PROFILE_URL"]?>" title="<?=GetMessage("AUTH_FORM_DEFAULT_PROFILE")?>">
                    <?=$arResult["USER_NAME"]?>
                </a>
                <?foreach ($arResult["GET"] as $key => $value):?>
                    <input type="hidden" name="<?=htmlspecialcharsbx($key)?>" value="<?=htmlspecialcharsbx($value)?>" />
                <?endforeach?>
                <input type="hidden" name="logout" value="yes" />
                <input class="auth-form-default-button auth-form-default-logout" type="submit" name="logout_butt" value="<?=GetMessage("AUTH_FORM_DEFAULT_LOGOUT")?>" />
            <?$frame->beginStub();?>
                <div class="uni-aligner-vertical"></div>
                <div class="auth-form-default-icon"></div>
                <a class="auth-form-default-button auth-form-default-login" onclick="return openAuthorizePopup();" title="<?=GetMessage("AUTH_FORM_DEFAULT_LOGIN")?>">
                    <?=GetMessage("AUTH_FORM_DEFAULT_LOGIN")?>
                </a>
            <?$frame->end();?>
        </form>
    <?endif;?>
</div>

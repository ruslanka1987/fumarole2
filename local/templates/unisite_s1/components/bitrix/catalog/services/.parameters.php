<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

if (!Loader::includeModule('iblock'))
	return;

$arTemplateParameters = array();

/* Отзывы */
$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$iblockFilter = (
	!empty($arCurrentValues['REVIEWS_IBLOCK_TYPE'])
	? array('TYPE' => $arCurrentValues['REVIEWS_IBLOCK_TYPE'], 'ACTIVE' => 'Y')
	: array('ACTIVE' => 'Y')
);
$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $iblockFilter);
while ($arr = $rsIBlock->Fetch())
	$arIBlock[$arr['ID']] = '['.$arr['ID'].'] '.$arr['NAME'];
unset($arr, $rsIBlock, $iblockFilter);

$arTemplateParameters['PROJECTS_SECTION_URL'] = array(
	"PARENT" => "",
	"NAME" => GetMessage("PROJECTS_SECTION_URL"),
	"TYPE" => "STRING"
);

$arTemplateParameters['REVIEWS_SECTION_URL'] = array(
	"PARENT" => "",
	"NAME" => GetMessage("REVIEWS_SECTION_URL"),
	"TYPE" => "STRING"
);

$arTemplateParameters['REVIEWS_IBLOCK_TYPE'] = array(
	"PARENT" => "REVIEW_SETTINGS",
	"NAME" => GetMessage("REVIEWS_IBLOCK_TYPE"),
	"TYPE" => "LIST",
	"VALUES" => $arIBlockType,
	"REFRESH" => "Y"
);

$arTemplateParameters['REVIEWS_IBLOCK_ID'] = array(
	"PARENT" => "REVIEW_SETTINGS",
	"NAME" => GetMessage("REVIEWS_IBLOCK_IBLOCK"),
	"TYPE" => "LIST",
	"ADDITIONAL_VALUES" => "Y",
	"VALUES" => $arIBlock,
	"REFRESH" => "Y"
);

$arTemplateParameters['REVIEWS_COUNT'] = array(
	"PARENT" => "REVIEW_SETTINGS",
	"NAME" => GetMessage("REVIEWS_COUNT"),
	"TYPE" => "STRING"
);

/* - Отзывы - */

$arTemplateParameters['USE_SIMILAR_SERVICES'] = array(
	"NAME" => GetMessage("USE_SIMILAR_SERVICES"),
	"TYPE" => "CHECKBOX",
	'DEFAULT' => 'N'
);

/* Заголовки */
$arTemplateParameters['ELEMENT_CAPTION_DESCRIPTION'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_DESCRIPTION"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_DESCRIPTION_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_SPECIALIST'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_SPECIALIST"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_SPECIALIST_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_DOCUMENTS'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_DOCUMENTS"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_DOCUMENTS_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_CHARACTERISTICS'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_CHARACTERISTICS"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_CHARACTERISTICS_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_GALERY'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_GALERY"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_GALERY_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_PROJECTS'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_PROJECTS"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_PROJECTS_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_REVIEWS'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_REVIEWS"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_REVIEWS_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_SERVICES'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_SERVICES"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_SERVICES_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_VIDEO'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_VIDEO"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_VIDEO_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_HOW_WE_WORK'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_HOW_WE_WORK"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_HOW_WE_WORK_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_OUR_ADVANTAGES'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_OUR_ADVANTAGES"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_OUR_ADVANTAGES_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_OUR_CLIENTS'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_OUR_CLIENTS"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_OUR_CLIENTS_DEFAULT")
);

$arTemplateParameters['ELEMENT_CAPTION_CONTACTS'] = array(
	"NAME" => GetMessage("SERVICES_ELEMENT_CAPTION_CONTACTS"),
	"TYPE" => "STRING",
	'DEFAULT' => GetMessage("SERVICES_ELEMENT_CAPTION_CONTACTS_DEFAULT")
);
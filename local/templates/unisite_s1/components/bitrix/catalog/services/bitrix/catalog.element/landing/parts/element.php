<?if ($sCurrentElement == 'CONTACT'):?>
    <?if ($arData['CONTACT']['SHOW']):?>
        <div class="uni-indents-vertical indent-45"></div>
        <div class="service-section">
            <?$APPLICATION->IncludeComponent("intec:custom.iblock.element", "contact.landing.1", Array(
            	   "IBLOCK_ELEMENT_ID" => $arData['CONTACT']['VALUE'],
                   "USE_DETAIL_PICTURE" => "N",
                   "USE_PREVIEW_PICTURE" => "N",
                   "DISPLAY_BUTTON" => "Y",
                   "BUTTON_CLASS" => 'orderService',
                   "BUTTON_TEXT" => GetMessage('SERVICE_HEADER_ORDER_BUTTON')
            	),
            	$component
            );?>
        </div>
    <?endif;?>
<?elseif ($sCurrentElement == 'FORM'):?>
	<div class="uni-indents-vertical indent-50"></div>
	<div class="form_order_serv">
		<div class="form_order_serv_wrapper">
			<?$APPLICATION->IncludeComponent(
				"intec:startshop.forms.result.new", 
				"order_service_landing", 
				array(
					"AJAX_MODE" => "Y",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "Y",
					"AJAX_OPTION_STYLE" => "Y",
					"FORM_ID" => $arParams['WEB_FORM_SERVICE_ID'],
					"FORM_VARIABLE_CAPTCHA_CODE" => "CAPTCHA_CODE",
					"FORM_VARIABLE_CAPTCHA_SID" => "CAPTCHA_SID",
					"REQUEST_VARIABLE_ACTION" => "action",
					"SERVICE_NAME" => $arResult["NAME"],
					"COMPONENT_TEMPLATE" => "order_service_landing"
				),
				$component
			);?>
		</div>
	</div>
	<div class="form_order_serv_size"></div>
	<script>
		$formOrderHeight = $('.service.landing .form_order_serv').outerHeight(false);
		$('.service.landing .form_order_serv_size').css('height', $formOrderHeight);
		
		$(window).resize(function() {
			$formOrderHeight = $('.service.landing .form_order_serv').outerHeight(false);
			$('.service.landing .form_order_serv_size').css('height', $formOrderHeight);
		});
	</script>
<?endif;?>
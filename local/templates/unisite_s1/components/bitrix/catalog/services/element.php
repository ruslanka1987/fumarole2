<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
global $options;
$this->setFrameMode(true);

$arFlags = array(
    "SHOW_LEFT_MENU" => ($options['SERVICES_VIEW']['ACTIVE_VALUE'] == "WITH_TABS" || $options['SERVICES_VIEW']['ACTIVE_VALUE'] == "WITHOUT_TABS")
);

$arTemplateAssociation = array(
    "WITH_TABS" => '.default',
    "WITHOUT_TABS" => '.default',
    "LANDING" => 'landing'
);

$sTemplate = $arTemplateAssociation[$options['SERVICES_VIEW']['ACTIVE_VALUE']];

?>
<?if ($arFlags['SHOW_LEFT_MENU']):?>
    <div class="left_col">
    
    	<?$APPLICATION->IncludeComponent(
    	"bitrix:catalog.section.list",
    	"",
    	array(
    		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
    		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
    		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
    		"CACHE_TIME" => $arParams["CACHE_TIME"],
    		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
    		"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
    		"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
    		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
    		"VIEW_MODE" => "MENU",
    		"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
    		"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
    		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
    	),
    	$component,
    	array("HIDE_ICONS" => "Y")
    );
    ?>
    </div>
    <div class="right_col">
<?endif;?>
<?
	$ElementID = $APPLICATION->IncludeComponent(
	"bitrix:catalog.element",
	$sTemplate,
	array(
		"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"PROPERTY_CODE" => $arParams["DETAIL_PROPERTY_CODE"],
		"META_KEYWORDS" => $arParams["DETAIL_META_KEYWORDS"],
		"META_DESCRIPTION" => $arParams["DETAIL_META_DESCRIPTION"],
		"BROWSER_TITLE" => $arParams["DETAIL_BROWSER_TITLE"],
		"SET_CANONICAL_URL" => $arParams["DETAIL_SET_CANONICAL_URL"],
		"BASKET_URL" => $arParams["BASKET_URL"],
		"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
		"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
		"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
		"CHECK_SECTION_ID_VARIABLE" => (isset($arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"]) ? $arParams["DETAIL_CHECK_SECTION_ID_VARIABLE"] : ''),
		"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
		"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
		"CACHE_TYPE" => $arParams["CACHE_TYPE"],
		"CACHE_TIME" => $arParams["CACHE_TIME"],
		"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
		"SET_TITLE" => $arParams["SET_TITLE"],
		"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
		"MESSAGE_404" => $arParams["MESSAGE_404"],
		"SET_STATUS_404" => $arParams["SET_STATUS_404"],
		"SHOW_404" => $arParams["SHOW_404"],
		"FILE_404" => $arParams["FILE_404"],
		"PRICE_CODE" => $arParams["PRICE_CODE"],
		"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
		"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],
		"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
		"PRICE_VAT_SHOW_VALUE" => $arParams["PRICE_VAT_SHOW_VALUE"],
		"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
		"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],
		"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
		"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
		"LINK_IBLOCK_TYPE" => $arParams["LINK_IBLOCK_TYPE"],
		"LINK_IBLOCK_ID" => $arParams["LINK_IBLOCK_ID"],
		"LINK_PROPERTY_SID" => $arParams["LINK_PROPERTY_SID"],
		"LINK_ELEMENTS_URL" => $arParams["LINK_ELEMENTS_URL"],
		"ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
		"ELEMENT_CODE" => $arResult["VARIABLES"]["ELEMENT_CODE"],
		"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
		"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
		"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
		"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
		'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
		'CURRENCY_ID' => $arParams['CURRENCY_ID'],
		'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
		'USE_ELEMENT_COUNTER' => $arParams['USE_ELEMENT_COUNTER'],
		'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
		"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],

		'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
		'LABEL_PROP' => $arParams['LABEL_PROP'],
		'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
		'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
		'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
		'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
		'SHOW_MAX_QUANTITY' => $arParams['DETAIL_SHOW_MAX_QUANTITY'],
		'MESS_BTN_BUY' => $arParams['MESS_BTN_BUY'],
		'MESS_BTN_ADD_TO_BASKET' => $arParams['MESS_BTN_ADD_TO_BASKET'],
		'MESS_BTN_SUBSCRIBE' => $arParams['MESS_BTN_SUBSCRIBE'],
		'MESS_BTN_COMPARE' => $arParams['MESS_BTN_COMPARE'],
		'MESS_NOT_AVAILABLE' => $arParams['MESS_NOT_AVAILABLE'],
		'USE_VOTE_RATING' => $arParams['DETAIL_USE_VOTE_RATING'],
		'VOTE_DISPLAY_AS_RATING' => (isset($arParams['DETAIL_VOTE_DISPLAY_AS_RATING']) ? $arParams['DETAIL_VOTE_DISPLAY_AS_RATING'] : ''),
		'USE_COMMENTS' => $arParams['DETAIL_USE_COMMENTS'],
		'BLOG_USE' => (isset($arParams['DETAIL_BLOG_USE']) ? $arParams['DETAIL_BLOG_USE'] : ''),
		'BLOG_URL' => (isset($arParams['DETAIL_BLOG_URL']) ? $arParams['DETAIL_BLOG_URL'] : ''),
		'BLOG_EMAIL_NOTIFY' => (isset($arParams['DETAIL_BLOG_EMAIL_NOTIFY']) ? $arParams['DETAIL_BLOG_EMAIL_NOTIFY'] : ''),
		'VK_USE' => (isset($arParams['DETAIL_VK_USE']) ? $arParams['DETAIL_VK_USE'] : ''),
		'VK_API_ID' => (isset($arParams['DETAIL_VK_API_ID']) ? $arParams['DETAIL_VK_API_ID'] : 'API_ID'),
		'FB_USE' => (isset($arParams['DETAIL_FB_USE']) ? $arParams['DETAIL_FB_USE'] : ''),
		'FB_APP_ID' => (isset($arParams['DETAIL_FB_APP_ID']) ? $arParams['DETAIL_FB_APP_ID'] : ''),
		'BRAND_USE' => (isset($arParams['DETAIL_BRAND_USE']) ? $arParams['DETAIL_BRAND_USE'] : 'N'),
		'BRAND_PROP_CODE' => (isset($arParams['DETAIL_BRAND_PROP_CODE']) ? $arParams['DETAIL_BRAND_PROP_CODE'] : ''),
		'DISPLAY_NAME' => (isset($arParams['DETAIL_DISPLAY_NAME']) ? $arParams['DETAIL_DISPLAY_NAME'] : ''),
		'ADD_DETAIL_TO_SLIDER' => (isset($arParams['DETAIL_ADD_DETAIL_TO_SLIDER']) ? $arParams['DETAIL_ADD_DETAIL_TO_SLIDER'] : ''),
		'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
		"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
		"ADD_ELEMENT_CHAIN" => (isset($arParams["ADD_ELEMENT_CHAIN"]) ? $arParams["ADD_ELEMENT_CHAIN"] : ''),
		"DISPLAY_PREVIEW_TEXT_MODE" => (isset($arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE']) ? $arParams['DETAIL_DISPLAY_PREVIEW_TEXT_MODE'] : ''),
		"DETAIL_PICTURE_MODE" => (isset($arParams['DETAIL_DETAIL_PICTURE_MODE']) ? $arParams['DETAIL_DETAIL_PICTURE_MODE'] : ''),
		'ADD_TO_BASKET_ACTION' => $basketAction,
		'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
		'DISPLAY_COMPARE' => (isset($arParams['USE_COMPARE']) ? $arParams['USE_COMPARE'] : ''),
		'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
		'SHOW_BASIS_PRICE' => (isset($arParams['DETAIL_SHOW_BASIS_PRICE']) ? $arParams['DETAIL_SHOW_BASIS_PRICE'] : 'Y'),
		'BACKGROUND_IMAGE' => (isset($arParams['DETAIL_BACKGROUND_IMAGE']) ? $arParams['DETAIL_BACKGROUND_IMAGE'] : ''),
		'USE_REVIEW' => $arParams['USE_REVIEW'],
		'REVIEWS_IBLOCK_TYPE' => $arParams['REVIEWS_IBLOCK_TYPE'],
		'REVIEWS_IBLOCK_ID' => $arParams['REVIEWS_IBLOCK_ID'],
		'REVIEWS_COUNT' => $arParams['REVIEWS_COUNT'],
		'USE_SIMILAR_SERVICES' => $arParams['USE_SIMILAR_SERVICES'],
        'PROJECTS_SECTION_URL' => $arParams['PROJECTS_SECTION_URL'],
        'REVIEWS_SECTION_URL' => $arParams['REVIEWS_SECTION_URL'],
        'ELEMENT_CAPTION_DESCRIPTION' => $arParams['ELEMENT_CAPTION_DESCRIPTION'],
        'ELEMENT_CAPTION_SPECIALIST' => $arParams['ELEMENT_CAPTION_SPECIALIST'],
        'ELEMENT_CAPTION_DOCUMENTS' => $arParams['ELEMENT_CAPTION_DOCUMENTS'],
        'ELEMENT_CAPTION_CHARACTERISTICS' => $arParams['ELEMENT_CAPTION_CHARACTERISTICS'],
        'ELEMENT_CAPTION_GALERY' => $arParams['ELEMENT_CAPTION_GALERY'],
        'ELEMENT_CAPTION_PROJECTS' => $arParams['ELEMENT_CAPTION_PROJECTS'],
        'ELEMENT_CAPTION_REVIEWS' => $arParams['ELEMENT_CAPTION_REVIEWS'],
        'ELEMENT_CAPTION_SERVICES' => $arParams['ELEMENT_CAPTION_SERVICES'],
        'ELEMENT_CAPTION_VIDEO' => $arParams['ELEMENT_CAPTION_VIDEO'],
        'ELEMENT_CAPTION_HOW_WE_WORK' => $arParams['ELEMENT_CAPTION_HOW_WE_WORK'],
        'ELEMENT_CAPTION_OUR_ADVANTAGES' => $arParams['ELEMENT_CAPTION_OUR_ADVANTAGES'],
        'ELEMENT_CAPTION_OUR_CLIENTS' => $arParams['ELEMENT_CAPTION_OUR_CLIENTS'],
        'ELEMENT_CAPTION_CONTACTS' => $arParams['ELEMENT_CAPTION_CONTACTS'],
		'PICTURE_BLOCK_HEIGHT' => $arParams['PICTURE_BLOCK_HEIGHT'],
		"WEB_FORM_QUESTION_SERVICE_ID" => $arParams['WEB_FORM_QUESTION_SERVICE_ID'],
		"WEB_FORM_SERVICE_ID" => $arParams['WEB_FORM_SERVICE_ID']
	),
	$component
);?>
<?if ($arFlags['SHOW_LEFT_MENU']):?>
    </div>
<?endif;?>
<div class="clear"></div>




<?$thisID  = 'services_bgn';?>
<?$thisID2 = 'services_bgn_size';?>
<div class="tiles_bgn" id="<?=$thisID?>">
	<div class="tiles_bgn_wrapper">
		<div class="uni-indents-vertical indent-25"></div>
		<div class="header-main">
			<div class="text">Услуги</div>
		</div>
		<div class="uni-indents-vertical indent-25"></div>
		<?$APPLICATION->IncludeComponent(
			"intec:custom.iblock.element.list", 
			"tiles.landing.1", 
			array(
				"COMPONENT_TEMPLATE" => "tiles.landing.1",
				"IBLOCK_TYPE" => "catalog",
				"IBLOCK_ID" => "28",
				"IBLOCK_ELEMENTS_ID" => array(
				),
				"PICTURE_WIDTH" => "600",
				"USE_DETAIL_PICTURE" => "N",
				"USE_PREVIEW_PICTURE" => "Y",
				"PICTURE_HEIGHT" => "600",
				"NO_PICTURE_PATH" => SITE_TEMPLATE_PATH."/images/noimg/no-img.png",
				"IBLOCK_SECTION" => "",
				"USE_LINK_TO_ELEMENTS" => "Y",
				"LINK_TO_ELEMENTS" => "",
				"PICTURE_BLOCK_HEIGHT" => "70%",
				"IBLOCK_SECTION_CODE" => "",
				"IBLOCK_ELEMENTS_COUNT" => "4",
				"IBLOCK_SORT_FIELD" => ""
			),
			false
		);?>
	</div>
</div>
<div class="tiles_bgn_size" id="<?=$thisID2?>"></div>
<script>
	$tilesHeight<?=$thisID?> = $('#<?=$thisID?>').outerHeight(false);
	$('#<?=$thisID2?>').css('height', $tilesHeight<?=$thisID?>);
	
	$(window).resize(function() {
		$tilesHeight<?=$thisID?> = $('#<?=$thisID?>').outerHeight(false);
		$('#<?=$thisID2?>').css('height', $tilesHeight<?=$thisID?>);
	});
</script>
<?
    $MESS["PROJECTS_SECTION_URL"] = "Адрес до раздела с проектами";
    $MESS["REVIEWS_SECTION_URL"] = "Адрес до раздела с отзывами";
    $MESS["REVIEWS_IBLOCK_TYPE"] = "Тип инфоблока отзывов";
    $MESS["REVIEWS_IBLOCK_IBLOCK"] = "Инфоблок с отзывами";
    $MESS["REVIEWS_COUNT"] = "Количество отзывов";
    $MESS["USE_SIMILAR_SERVICES"] = "Отображать похожие услуги";
    $MESS["LINE_SECTION_COUNT"] = "Количество элементов в одной строке";
    
    $MESS["SERVICES_ELEMENT_CAPTION_DESCRIPTION"] = "Заголовок описания";
    $MESS["SERVICES_ELEMENT_CAPTION_DESCRIPTION_DEFAULT"] = "Описание";
    $MESS["SERVICES_ELEMENT_CAPTION_SPECIALIST"] = "Заголовок специалиста";
    $MESS["SERVICES_ELEMENT_CAPTION_SPECIALIST_DEFAULT"] = "Специалист";
    $MESS["SERVICES_ELEMENT_CAPTION_DOCUMENTS"] = "Заголовок документов";
    $MESS["SERVICES_ELEMENT_CAPTION_DOCUMENTS_DEFAULT"] = "Документы";
    $MESS["SERVICES_ELEMENT_CAPTION_CHARACTERISTICS"] = "Заголовок характеристик";
    $MESS["SERVICES_ELEMENT_CAPTION_CHARACTERISTICS_DEFAULT"] = "Технические характеристики";
    $MESS["SERVICES_ELEMENT_CAPTION_GALERY"] = "Заголовок галереи";
    $MESS["SERVICES_ELEMENT_CAPTION_GALERY_DEFAULT"] = "Галерея";
    $MESS["SERVICES_ELEMENT_CAPTION_PROJECTS"] = "Заголовок проектов";
    $MESS["SERVICES_ELEMENT_CAPTION_PROJECTS_DEFAULT"] = "Проекты";
    $MESS["SERVICES_ELEMENT_CAPTION_REVIEWS"] = "Заголовок отзывов";
    $MESS["SERVICES_ELEMENT_CAPTION_REVIEWS_DEFAULT"] = "Отзывы";
    $MESS["SERVICES_ELEMENT_CAPTION_SERVICES"] = "Заголовок сопутствующих услуг";
    $MESS["SERVICES_ELEMENT_CAPTION_SERVICES_DEFAULT"] = "Сопутствующие услуги";
    $MESS["SERVICES_ELEMENT_CAPTION_VIDEO"] = "Заголовок видео";
    $MESS["SERVICES_ELEMENT_CAPTION_VIDEO_DEFAULT"] = "Видео";
    $MESS["SERVICES_ELEMENT_CAPTION_HOW_WE_WORK"] = "Заголовок как мы работаем";
    $MESS["SERVICES_ELEMENT_CAPTION_HOW_WE_WORK_DEFAULT"] = "Как мы работаем";
    $MESS["SERVICES_ELEMENT_CAPTION_OUR_ADVANTAGES"] = "Заголовок наших преимуществ";
    $MESS["SERVICES_ELEMENT_CAPTION_OUR_ADVANTAGES_DEFAULT"] = "Наши преимущества";
    $MESS["SERVICES_ELEMENT_CAPTION_OUR_CLIENTS"] = "Заголовок наших клиентов";
    $MESS["SERVICES_ELEMENT_CAPTION_OUR_CLIENTS_DEFAULT"] = "Наши клиенты";
    $MESS["SERVICES_ELEMENT_CAPTION_CONTACTS"] = "Заголовок контактов";
    $MESS["SERVICES_ELEMENT_CAPTION_CONTACTS_DEFAULT"] = "Контакты";
?>
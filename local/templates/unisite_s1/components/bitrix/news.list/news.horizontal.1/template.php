<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true)?>
<div class="news-horizontal-1">
    <div class="news-row">
        <?foreach ($arResult['ITEMS'] as $arItem):?>
            <?
                $sListPageUrl = $arItem['LIST_PAGE_URL'];
			    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                $date = (!empty($arItem['ACTIVE_FROM']))? $arItem['ACTIVE_FROM'] : $arItem['TIMESTAMP_X'];
                $anonsPict =  CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], Array("width" => 271, "height" => 190), BX_RESIZE_IMAGE_EXACT, false);
			?>
            <div class="item">
                <div class="box">
                    <div class="title">
                        <?=$arItem['NAME']?>
                    </div>
                    <div class="pict-wrap">
                        <div class="pict">
                            <img src="<?=$anonsPict['src']?>" alt="<?=$arItem['NAME']?>">
                        </div>
                    </div>
                    <div class="desc">
                        <?=substr(strip_tags($arItem['DETAIL_TEXT']), 0, 120)?>...
                    </div>
                    <div class="date"><?=FormatDate('j F Y', strtotime($date))?></div>
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="lnk-abs"></a>
                </div>
            </div>

        <?endforeach;?>
    </div>
    <?if (!empty($sListPageUrl)):?>
        <div class="news-horizontal-1-buttons">
            <a href="<?=$sListPageUrl?>" class="uni-button uni-button-gray news-horizontal-1-button-all button-bg">
                <?=GetMessage('NEWS_HORIZONTAL_BUTTONS_SHOW_ALL')?>
            </a>
        </div>
    <?endif;?>
</div>

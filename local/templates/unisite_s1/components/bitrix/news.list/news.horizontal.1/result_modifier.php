<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?
    foreach ($arResult['ITEMS'] as &$arItem) {
        $sPicture = "";
        
        if (!empty($arItem['DETAIL_PICTURE'])) {
            $sPicture = $arItem['DETAIL_PICTURE']['ID'];
        } else if (!empty($arItem['PREVIEW_PICTURE'])) {
            $sPicture = $arItem['PREVIEW_PICTURE']['ID'];
        }
        
        if (!empty($sPicture)) {
            $sPicture = CFile::ResizeImageGet($sPicture, array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
            $sPicture = $sPicture['src'];
        }
        
        if (empty($sPicture)) {
            $sPicture = SITE_TEMPLATE_PATH.'/images/noimg/no-img.png';
        }
        
        $arItem['PICTURE'] = $sPicture;
    }
?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $options;
$this->setFrameMode(true);?>
<?if(is_array($arResult["ITEMS"])){?>	
<div style="overflow:hidden;<?=$options['HEADER_WIDTH_SIZE']['ACTIVE_VALUE'] != 'Y'?' max-width: 1162px; margin: 0 auto;':''?>">
	<ul class="bx_slider">
		<?foreach($arResult["ITEMS"] as $arItem):
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<?$background = "none";
			if($arItem["PROPERTIES"]["BACKGROUND"]["VALUE"]){
				$background = CFile::GetFileArray($arItem["PROPERTIES"]["BACKGROUND"]["VALUE"]);				
			}
			if($background["HEIGHT"]!=0 && $background["WIDTH"]!=0) {
				$height = $background["HEIGHT"]/$background["WIDTH"]*100;
			}
			$position = $arItem["PROPERTIES"]["POSITION"]["VALUE_XML_ID"];
			$picture = $arItem["PROPERTIES"]["PICTURE"]["VALUE"];			
			if($picture) {
				$picture = CFile::ResizeImageGet($picture, Array("width" => 741, "height" => 412));		
				$picture = $picture["src"];	
			}
			$color_text = $arItem["PROPERTIES"]["COLOR_TEXT"]["VALUE_XML_ID"];
			$target = $arItem["PROPERTIES"]["TARGET"]["VALUE_XML_ID"];
			$href = $arItem["PROPERTIES"]["BANNER_HREF"]["VALUE"];?>
			<li class="slider_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<div class="sl_background" style="background-image:url(<?="'".$background["SRC"]."'"?>);">				
					<div class="sl_wrapper">
						<?if($href){?>
							<a href="<?=$href?>" target="<?=$target?>" style="text-decoration:none">
						<?}?>
							<table>
								<tr>
									<?if($position == "picture" || $position == "onlytext"){?>
										<td class="full">
											<?if($position == "picture"){?>
                                                <div style="padding-left: 20px; padding-right: 20px;">
												    <img class="bx_slide_img" src = "<?=$picture?>">
                                                </div>
											<?}?>
											<?if($position == "onlytext"){?>
                                                <div style="padding-left: 20px; padding-right: 20px;">
    												<?if($arItem["PROPERTIES"]["HEADER"]["~VALUE"]){?>
    													<div class = "sl_header1 <?=$color_text?>">
    														<?=$arItem["PROPERTIES"]["HEADER"]["VALUE"]?>
    													</div>
    												<?}?>
    												<?if($arItem["PROPERTIES"]["HEADER2"]["VALUE"]){?>
    													<div class = "sl_header2 <?=$color_text?>">
    														<?=$arItem["PROPERTIES"]["HEADER2"]["VALUE"]?>
    													</div>
    												<?}?>
    												<?if($arItem["PROPERTIES"]["TEXT"]["VALUE"]){?>
    													<div class = "sl_text <?=$color_text?>">
    														<?=$arItem["PROPERTIES"]["TEXT"]["VALUE"]["TEXT"]?>
    													</div>
    												<?}?>
                                                </div>
											<?}?>
										</td>
									<?}else{?>
										<?if($position == "right"){?>
											<td class="image">
                                                <?if ($picture):?>
                                                    <div style="padding-left: 20px; padding-right: 40px;">
												        <img class="bx_slide_img" src = "<?=$picture?>">
                                                    </div>
                                                <?endif;?>
											</td>
										<?}?>
										<td>
                                            <div style="padding-left: 10px; padding-right: 10px;">								
    											<?if($arItem["PROPERTIES"]["HEADER"]["VALUE"]){?>
    												<div class = "sl_header1 <?=$color_text?>">
    													<?=$arItem["PROPERTIES"]["HEADER"]["VALUE"]?>
    												</div>
    											<?}?>
    											<?if($arItem["PROPERTIES"]["HEADER2"]["VALUE"]){?>
    												<div class = "sl_header2 <?=$color_text?>">
    													<?=$arItem["PROPERTIES"]["HEADER2"]["VALUE"]?>
    												</div>
    											<?}?>
    											<?if($arItem["PROPERTIES"]["TEXT"]["VALUE"]){?>
    												<div class = "sl_text <?=$color_text?>">
    													<?=$arItem["PROPERTIES"]["TEXT"]["VALUE"]["TEXT"]?>
    												</div>
    											<?}?>
                                            </div>	
										</td>
										<?if($position == "left"){?>
											<td class="image">
                                                <?if ($picture):?>
                                                    <div style="padding-left: 40px; padding-right: 20px;">
												        <img class="bx_slide_img" src = "<?=$picture?>">
                                                    </div>
                                                <?endif;?>
											</td>
										<?}?>
									<?}?>
								</tr>			
							</table>
						<?if($href){?>
							</a>
						<?}?>
						<div class="clear"></div>
					</div>
				</div>
			</li>
		<?endforeach;?>
	</ul>
</div>
	<script>
	$(document).ready(function(){
		//$('.slider').intecSlider();	
		var slider = $('.bx_slider').bxSlider({
			mode : "<?=$arParams["MODE_SLIDER"]?>",
			speed: "<?=$arParams["SPEED_SLIDER"]?>",
			pager: true,
			auto: <?=$arParams["USE_AUTOSCROLL"] == "Y"? "true":"false"?>,
			pause: "<?=$arParams["PAUSE_AUTOSCROLL"]?>",			
			onSlideAfter: function(currentSlide, totalSlides, currentSlideHtmlObject){
				$(currentSlideHtmlObject).find(".bx_slide_img").fadeIn("slow");
			},
			
		});
	//slider.onSlideAfter(function(){console.log(123);});
	});
	</script>
<?}?>
<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();?>
<?$this->setFrameMode(true)?>
<?
if (!function_exists("drawSectionsUlLi1")){
	function drawSectionsUlLi1 ($sections, $classes) {
		$prevLevel = 0;
		$count = 0;
		
		?>
		<div class="submenu_mobile">						
			<?foreach ($sections as $section) {?>
				<?if($section["DEPTH_LEVEL"]==1) {?>
					<a class="hover_link" href="<?=$section["SECTION_PAGE_URL"]?>"><?=$section["NAME"]?></a>
				<?}?>
				
			<?}?>
		</div>
		<?
		foreach ($sections as $section) {
			$name = $section["NAME"];
			$level = $section["DEPTH_LEVEL"];
			$currentClasses = ($level > (count($classes)-1)) ? $classes[count($classes)-1] : $classes[$level];
			if ($level == $prevLevel) {
				echo str_repeat("\t", $level)."</li>\n";
			} elseif ($level < $prevLevel) {
				echo str_repeat("</li>\n</ul>\n", $prevLevel-$level)."</li>\n";
			} else {
				echo "\n".str_repeat("\t", $level)."<ul";
					if(!empty($currentClasses["ul"])) {
						echo " class=\"".$currentClasses["ul"]."\"";
					}
				echo ">\n";
			}
			echo str_repeat("\t", $level)."<li";
				if(!empty($currentClasses["li"])) {
					echo " class=\"".$currentClasses["li"]."\"";
				}
			echo ">";
			if($level == "2" && $prevLevel == $level){
				$count++;
			}else{
				$count=0;
			}
			$active = 0;			
			if (CSite::InDir($section['SECTION_PAGE_URL'])){
				$active = 1;		
			}
			$hide = 0;
			if ($count >=  4){
				$hide = 1;
			}
			if($count == 4){?>
				<div class="show_all"> 
					<span>
						<?=GetMessage("READ_MORE")?>
					</span>
				</div>
			<?}?>
				<a class="<?=$hide?"hide":""?> <?=$active?"active":""?> hover_link" href="<?=$section["SECTION_PAGE_URL"]?>"><?=$name?></a>			
			<?$prevLevel = $level;
		}
		echo str_repeat("</li>\n</ul>\n", $prevLevel);
	}
}
?>

<?
if(!empty($arResult)) {
	$classes = array(
		0 => array(
			"li"=>"root",
		),
		1 => array(
			"li"=>"root",
			"ul"=>"i_menu",
		),
		2 => array(
			"li"=>"",
			"ul"=>"submenu_1",
		),
		3 => array(
			"li"=>"",
			"ul"=>"submenu_2",
		),
	);
	if(is_array($arResult["SECTIONS"])){
		drawSectionsUlLi1($arResult["SECTIONS"], $classes);	
	}	
}?>
<script>
	$(document).ready(
		function(){
			$(".i_menu .show_all").click(function(){	
				$(".i_menu .submenu_1").removeClass("bordered");
				$(this).parent().parent().addClass("bordered");
				$(".i_menu .show_all").hide();
			})			
		}
	)
	$(document).on("click",function(){
		$(".i_menu .show_all").show();
		$(".i_menu .submenu_1").removeClass("bordered");
	})
	$(document).on("click",".i_menu .submenu_1.bordered",function(e){		
		e.stopPropagation();		
	})
</script>
<?if (!defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED !== true) die();?>
<?
    $arDefaultParams = array(
        "ELEMENTS_COUNT" => "4",
        "SECTIONS" => array()
    );
    
    $arParams = array_merge($arDefaultParams, $arParams);
    $bSectionsFilterNotEmpty = false;
    
    if (is_array($arParams['SECTIONS']) && !empty($arParams['SECTIONS']))
        foreach ($arParams['SECTIONS'] as $iSectionID)
            if (!empty($iSectionID)) {
                $bSectionsFilterNotEmpty = true;
                break;
            }
    
    if ($bSectionsFilterNotEmpty)
        foreach ($arResult['SECTIONS'] as $sKey => $arSection)
            if (!in_array($arSection['ID'], $arParams['SECTIONS']))
                unset($arResult['SECTIONS'][$sKey]);
    
    $iSectionsCount = intval($arParams['ELEMENTS_COUNT']);
    $arResult['SECTIONS'] = array_slice($arResult['SECTIONS'], 0, $iSectionsCount);
    
    foreach ($arResult['SECTIONS'] as &$arSection) {
        $sPicture = SITE_TEMPLATE_PATH.'/images/noimg/no-img.png';
        
        if (!empty($arSection['PICTURE']))
            $sPicture = $arSection['PICTURE']['SRC'];

        $arSection['PICTURE'] = $sPicture;
    }

//echo "<pre>"; print_r($arResult);echo "</pre>";

/*$uf_name = Array("UF_SEO_TEXT");

	foreach ($arResult['SECTIONS'] as $key=>$arSection) {
		 	$rsSections = CIBlockSection::GetList(array(), array('IBLOCK_ID'=>$arParams['IBLOCK_ID'], 'ID'=>$arSection['ID']),false, $uf_name);
		   	while ($arS = $rsSections->Fetch()) {
	          	 $arResult['SECTIONS'][$key]['UF_SEO_TEXT'] = $arS['UF_SEO_TEXT'];
        		}
}*/
?>
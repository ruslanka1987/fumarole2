<?if (!defined("B_PROLOG_INCLUDED") && B_PROLOG_INCLUDED !== true) die();?>
<?
    if (!CModule::IncludeModule('iblock'))
        return;
        
    $arSectionsFilter = array();
    $arSections = array();
    
    if (!empty($arCurrentValues['IBLOCK_TYPE']) && !empty($arCurrentValues['IBLOCK_ID'])) {
        $arSectionsFilter['IBLOCK_TYPE'] = $arCurrentValues['IBLOCK_TYPE'];
        $arSectionsFilter['IBLOCK_ID'] = $arCurrentValues['IBLOCK_ID'];
        $arSectionsFilter['ACTIVE'] = "Y";
        
        if (!empty($arCurrentValues['SECTION_ID']))
            $arSectionsFilter['SECTION_ID'] = $arCurrentValues['SECTION_ID'];
            
        $rsSections = CIBlockSection::GetList(array(), $arSectionsFilter);
        
        while ($arSection = $rsSections->GetNext())
            $arSections[$arSection['ID']] = '['.$arSection['ID'].'] '.$arSection['NAME'];
    }

    $arTemplateParameters = array(
        "ELEMENTS_COUNT" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("SECTIONS_ELEMENTS_COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => "4"
        ),
        "SECTIONS" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage('SECTIONS_SECTIONS'),
            "TYPE" => "LIST",
            "MULTIPLE" => "Y",
            "ADDITIONAL_VALUES" => "Y",
            "VALUES" => $arSections,
            "SIZE" => "10"
        )
    );
?>
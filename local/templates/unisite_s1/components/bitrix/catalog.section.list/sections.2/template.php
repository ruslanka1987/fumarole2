<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true)?>
<div class="tiles landing-1">
    <div class="sections-2 ">
        <div class="tiles-wrapper">
            <?foreach ($arResult['SECTIONS'] as $arSection):?>
                <?
                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <div class="tiles-tile">
                    <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="tiles-tile-wrapper">
                        <div class="tiles-tile-image" style="padding-top: 70%;">
                            <div class="tiles-tile-image-wrapper">
                                    <div class="uni-image">
                                        <img src="<?=$arSection['PICTURE']?>" alt="<?=htmlspecialcharsbx($arSection['NAME'])?>" title="<?=htmlspecialcharsbx($arSection['NAME'])?>"/>
                                    </div>
                            </div>
                        </div>
                        <div class="tiles-tile-information">
                            <div class="tiles-tile-information-name">
                                <?=$arSection['NAME']?>
                            </div>
                            <div class="uni-indents-vertical indent-5"></div>
                            <div class="tiles-tile-information-description">
                                <?=$arSection['UF_SEO_TEXT']?>
                            </div>
                        </div>
                    </a>
                </div>

            <?endforeach;?>
        </div>
    </div>
    <div class="uni-indents-vertical indent-40"></div>
    <!-- <div class="tiles-buttons">
        <a href="#" class="uni-button btn- tiles-buttons-more  uni-button-gray button-bg">Смотреть еще</a>
    </div>-->
</div>
<!--  end tiles landing-1-->

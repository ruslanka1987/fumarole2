<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<?$this->setFrameMode(true)?>
<div class="sections-1">
    <div class="sections-1-wrapper uni_parent_col">
        <?foreach ($arResult['SECTIONS'] as $arSection):?>
            <?
			    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
			    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
            <div class="sections-1-section uni_col uni-33">
                <div class="sections-1-border" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                    <div class="sections-1-border-wrapper">
                        <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="sections-1-image uni-image">
                            <div class="uni-aligner-vertical"></div>
                            <img src="<?=$arSection['PICTURE']?>" alt="<?=htmlspecialcharsbx($arSection['NAME'])?>" title="<?=htmlspecialcharsbx($arSection['NAME'])?>" />
                        </a>
                        <div class="sections-1-information">
                            <a href="<?=$arSection['SECTION_PAGE_URL']?>" class="sections-1-name">
                                <?=$arSection['NAME']?>
                            </a>
                            <div class="sections-1-description">
                                <?=$arSection['DESCRIPTION']?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?endforeach;?>
    </div>
</div>
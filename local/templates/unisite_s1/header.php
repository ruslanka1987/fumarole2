<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<?IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");?>
<?
    CJSCore::Init(array('fx', 'popup', 'ajax'));

    if(CModule::IncludeModule("intec.unisite")) {
		UniSite::InitProtection();
		UniSite::ShowInclude(SITE_ID);
	} else {
		die();
	}

    $options = UniSite::getOptionsValue(SITE_ID);

    $bIsFrontPage = false;
	$bShowMenuLeft = false;

    if (CSite::InDir(SITE_DIR.'index.php'))
		$bIsFrontPage = true;

    $oMenuLeft = new CMenu("left");
	$oMenuLeft->Init($APPLICATION->GetCurDir(), true);
	$bShowMenuLeft = (count($oMenuLeft->arMenu) > 0);

	$oMenuTop = new CMenu("topcustom");
	$oMenuTop->Init($APPLICATION->GetCurDir(), true);
	$bShowMenuTop = (count($oMenuTop->arMenu) > 0);

    $arMenuClasses = array();

    if ($options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] == 'header') $arMenuClasses[] = 'with-menu';
    if ($options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] == 'header' && $options["TYPE_PHONE"]["ACTIVE_VALUE"] == 'header') $arMenuClasses[] = 'with-phone';
    if ($options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] == 'catalog') $arMenuClasses[] = 'with-top-menu';

    $arMenuClasses = implode(' ', $arMenuClasses);

	$sFacebookContent = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_facebook.php");
	$sTwitterContent = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_twitter.php");
	$sVkontakteContent = $APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"].SITE_DIR."include/socnet_vk.php");

	$bShowFacebook = !empty($sFacebookContent);
	$bShowTwitter = !empty($sTwitterContent);
	$bShowVkontakte = (LANGUAGE_ID == 'ru' && !empty($sVkontakteContent));
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
    <head>
    	<title><?$APPLICATION->ShowTitle()?></title>
    	<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_DIR?>favicon.ico" />
        <?if ($options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'):?>
            <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width"/>
        <?endif;?>

    	<?$APPLICATION->ShowHead();?>
        <?$APPLICATION->IncludeComponent(
        	"intec:buttons.min.updater.lite",
        	".default",
        	array(
        		"COMPONENT_TEMPLATE" => ".default",
        		"IBLOCK_TYPE" => "catalog",
        		"IBLOCK_ID" => "12",
        		"COMPARE_NAME" => "CATALOG_COMPARE_LIST",
        		"COMPARE_ADD_MASK" => ".addToCompare#ID#",
        		"COMPARE_ADDED_MASK" => ".removeFromCompare#ID#",
        		"COMPARE_ADD_SELECTOR" => ".addToCompare",
        		"COMPARE_ADDED_SELECTOR" => ".removeFromCompare",
				"BASKET_ADD_MASK" => ".addToBasket#ID#",
				"BASKET_ADDED_MASK" => ".addedToBasket#ID#",
				"BASKET_ADD_SELECTOR" => ".addToBasket",
				"BASKET_ADDED_SELECTOR" => ".addedToBasket"
        	),
        	false,
            array('HIDE_ICONS' => 'Y')
        );?>
        <?
        $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/slick.css");

        $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . '/js/slick.min.js');
        ?>
    	<script type="text/javascript">
    		$(document).ready(function(){
    			resize();

    			function resize() {
    				var size = $('.bg_footer').outerHeight();
    				$('body').css('padding-bottom', (size + 50) + 'px');
    			}

                $(window).resize(function(){
                    resize();
                })
    		})
    	</script>
    </head>
    <body class="<?=$options['ADAPTIV']['ACTIVE_VALUE'] == 'Y'?'adaptiv':'no-adaptiv'?>">
        <?$APPLICATION->IncludeComponent(
    		"intec:panel.unisite.themeselector",
    		".default",
    		array(
    			"COMPONENT_TEMPLATE" => ".default"
    		),
    		$component,
    		array('HIDE_ICONS' => 'Y', 'ACTIVE_COMPONENT' => 'N')
    	);?>
        <div id="panel"><?$APPLICATION->ShowPanel();?></div>
        <div class="wrap">
        	<div class="header_wrap">
        		<div class="header_wrap_information">
        			<table class="header_wrap_container <?=$arMenuClasses?>">
        				<tr>
        					<td class="logo_wrap">
        						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        								"AREA_FILE_SHOW" => "file",
        								"PATH" => SITE_DIR."include/logo.php",
        							)
        						);?>
        					</td>
							<?if($options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] !== "header") { ?>
							<td class="desc_wrap">
								<div class="brd">
        						<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
        								"AREA_FILE_SHOW" => "file",
        								"PATH" => SITE_DIR."include/description.php",
        							)
        						);?>
								</div>
        					</td>
							<?}?>
        					<td class="right_wrap">
        						<table class="table_wrap">
        							<tr>
        								<?if($options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] == "header") { ?>
        									<td style="width: 100%;">
                                                <div class="menu_wrap">
        										<?$APPLICATION->IncludeComponent(
													"bitrix:menu",
													"top_horizontal_menu",
													array(
														"ROOT_MENU_TYPE" => "top",
														"MENU_CACHE_TYPE" => "A",
														"MENU_CACHE_TIME" => "36000000",
														"MENU_CACHE_USE_GROUPS" => "Y",
														"MENU_CACHE_GET_VARS" => array(
														),
														"CACHE_SELECTED_ITEMS" => "N", // Не создавать кеш меню для каждой страницы
														"MAX_LEVEL" => "3",
														"CHILD_MENU_TYPE" => "left",
														"USE_EXT" => "Y",
														"DELAY" => "N",
														"ALLOW_MULTI_SELECT" => "N",
														"IBLOCK_CATALOG_TYPE" => "catalog",
														"IBLOCK_CATALOG_ID" => "12",
														"IBLOCK_CATALOG_DIR" => SITE_DIR."catalog/",
														"MENU_IN" => "in-header",
														"WIDTH_MENU" => $options["WIDTH_MENU"]["ACTIVE_VALUE"],
														"COMPONENT_TEMPLATE" => "top_horizontal_menu",
														"IBLOCK_SERVICES_TYPE" => "catalog",
														"IBLOCK_SERVICES_ID" => "20",
														"IBLOCK_SERVICES_DIR" => SITE_DIR."product/"
													),
													false
												);?>
                                                </div>
        									</td>
        								<?}?>

        								<?if($options["TYPE_PHONE"]["ACTIVE_VALUE"] == 'header') {?>
        									<td>
        										<div class="phone_wrap">
                                                    <div class="phone-block">
                                                        <div class="phone">
            												<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
            														"AREA_FILE_SHOW" => "file",
            														"PATH" => SITE_DIR."include/company_phone.php",
            													)
            												);?>
            											</div>
                                                    </div>
                                                    <div class="info-block">
                                                        <a class="uni-button solid_button" href="/presentation.pdf" target="_blank">Скачать презентацию</a>
                                                        <div class="mail">
                                                            <a href="mailto:info@sp-vostok.com">info@sp-vostok.com</a>
                                                        </div>

                                                    </div>
        										</div>
        									</td>
        								<?} else {?>
											<td class="phone_wrap_mobile">
        										<div class="phone_wrap">
        											<div class="phone-block">
                                                        <div class="phone">
            												<?$APPLICATION->IncludeComponent("bitrix:main.include","",Array(
            														"AREA_FILE_SHOW" => "file",
            														"PATH" => SITE_DIR."include/company_phone.php",
            													)
            												);?>
            											</div>
                                                        <div class="order-call">
            												<span class="order-call solid_text" onclick="openCallForm('<?=SITE_DIR?>')"><?=GetMessage("CALL_TEXT")?></span>
            											</div>
                                                    </div>

        										</div>
        									</td>
										<?}?>
                                        <?if ($options["TYPE_BASKET"]["ACTIVE_VALUE"] == "header"):?>
        									<td>
        										<div class="basket_wrap<?=$options["TYPE_BASKET"]["ACTIVE_VALUE"] == "fly"?' fly':''?>">
        											<div class="b_compare">
                                                        <?$APPLICATION->IncludeComponent(
                                                        	"bitrix:catalog.compare.list",
                                                        	"compare.small",
                                                        	array(
                                                        		"COMPONENT_TEMPLATE" => "top",
                                                        		"IBLOCK_TYPE" => "catalog",
                                                        		"IBLOCK_ID" => "12",
                                                        		"AJAX_MODE" => "N",
                                                        		"AJAX_OPTION_JUMP" => "N",
                                                        		"AJAX_OPTION_STYLE" => "Y",
                                                        		"AJAX_OPTION_HISTORY" => "N",
                                                        		"AJAX_OPTION_ADDITIONAL" => "",
                                                        		"DETAIL_URL" => "",
                                                        		"COMPARE_URL" => SITE_DIR."catalog/compare.php",
                                                        		"NAME" => "CATALOG_COMPARE_LIST",
                                                        		"ACTION_VARIABLE" => "action",
                                                        		"PRODUCT_ID_VARIABLE" => "id",
                                                                "TYPE" => $options["TYPE_BASKET"]["ACTIVE_VALUE"]
                                                        	),
                                                        	false
                                                        );?>
                                                    </div>
                                                    <div class="b_basket">
        												<?$APPLICATION->IncludeComponent(
                                                        	"intec:startshop.basket.basket.small",
                                                        	".default",
                                                        	array(
                                                        		"DISPLAY_COUNT" => "Y",
																"DISPLAY_COUNT_IF_EMPTY" => "N",
																"DISPLAY_TOTAL" => "Y",
																"CURRENCY" => "rub",
																"URL_BASKET" => SITE_DIR."cart/",
																"REQUEST_VARIABLE_ACTION" => "action",
																"REQUEST_VARIABLE_ITEM" => "item",
																"REQUEST_VARIABLE_QUANTITY" => "quantity"
                                                        	),
                                                        	false
                                                        );?>
        											</div>
        										</div>
        									</td>
        								<?endif;?>
                                        <?if ($options["TYPE_BASKET"]["ACTIVE_VALUE"] == "fly"):?>
                                            <td>
                                                <div class="basket_wrap fly">
                                                    <div class="b_compare">
                                                        <?$APPLICATION->IncludeComponent(
                                                        	"bitrix:catalog.compare.list",
                                                        	"compare.small",
                                                        	array(
                                                        		"COMPONENT_TEMPLATE" => "top",
                                                        		"IBLOCK_TYPE" => "catalog",
                                                        		"IBLOCK_ID" => "12",
                                                        		"AJAX_MODE" => "N",
                                                        		"AJAX_OPTION_JUMP" => "N",
                                                        		"AJAX_OPTION_STYLE" => "Y",
                                                        		"AJAX_OPTION_HISTORY" => "N",
                                                        		"AJAX_OPTION_ADDITIONAL" => "",
                                                        		"DETAIL_URL" => "",
                                                        		"COMPARE_URL" => SITE_DIR."catalog/compare.php",
                                                        		"NAME" => "CATALOG_COMPARE_LIST",
                                                        		"ACTION_VARIABLE" => "action",
                                                        		"PRODUCT_ID_VARIABLE" => "id",
                                                                "TYPE" => $options["TYPE_BASKET"]["ACTIVE_VALUE"]
                                                        	),
                                                        	false
                                                        );?>
                                                    </div>
                                                    <div class="b_basket">
                                                        <div class="showable">
                                                            <?$APPLICATION->IncludeComponent(
                                                            	"intec:startshop.basket.basket.small",
                                                            	".default",
                                                            	array(
                                                            		"DISPLAY_COUNT" => "Y",
																	"DISPLAY_COUNT_IF_EMPTY" => "N",
																	"DISPLAY_TOTAL" => "Y",
																	"CURRENCY" => "rub",
																	"URL_BASKET" => SITE_DIR."cart/",
																	"REQUEST_VARIABLE_ACTION" => "action",
																	"REQUEST_VARIABLE_ITEM" => "item",
																	"REQUEST_VARIABLE_QUANTITY" => "quantity"
                                                            	),
                                                            	false
                                                            );?>
                                                        </div>
                                                        <div class="hideable" style="white-space: normal;">
                                                            <?$APPLICATION->IncludeComponent(
                                                            	"intec:startshop.basket.basket.small",
                                                            	".flying",
                                                            	array(
                                                            		"URL_BASKET" => SITE_DIR."cart/",
                                                            		"COMPONENT_TEMPLATE" => ".flying",
                                                            		"DISPLAY_GO_BUY" => "Y",
                                                            		"CURRENCY" => "",
                                                            		"URL_ORDER" => SITE_DIR."cart/?page=order",
																	"CFO_USE_FASTORDER" => "Y",
																	"CFO_PROP_NAME" => "NAME",
																	"CFO_PROP_PHONE" => "PHONE",
																	"CFO_PROP_COMMENT" => "COMMENT"
                                                            	),
                                                            	false
                                                            );?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        <?endif;?>
        							</tr>
        						</table>
        					</td>
        				</tr>
        			</table>
        		</div>
        		<div class="top <?=$arMenuClasses?>" style="<?=$options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] != "top"?'display: none':''?>">
        			<?$APPLICATION->IncludeComponent(
						"bitrix:menu",
						"top_horizontal_menu",
						array(
							"ROOT_MENU_TYPE" => "top",
							"MENU_CACHE_TYPE" => "A",
							"MENU_CACHE_TIME" => "36000000",
							"MENU_CACHE_USE_GROUPS" => "Y",
							"MENU_CACHE_GET_VARS" => array(
							),
							"CACHE_SELECTED_ITEMS" => "N", // Не создавать кеш меню для каждой страницы
							"MAX_LEVEL" => "3",
							"CHILD_MENU_TYPE" => "left",
							"USE_EXT" => "Y",
							"DELAY" => "N",
							"ALLOW_MULTI_SELECT" => "N",
							"IBLOCK_CATALOG_TYPE" => "catalog",
							"IBLOCK_CATALOG_ID" => "12",
							"IBLOCK_CATALOG_DIR" => SITE_DIR."catalog/",
							"MENU_IN" => "after-header",
							"TYPE_MENU" => $options["TYPE_TOP_MENU"]["ACTIVE_VALUE"],
							"MENU_WIDTH_SIZE" => $options["MENU_WIDTH_SIZE"]["ACTIVE_VALUE"],
							"SMOOTH_COLUMNS" => "Y",
							"COMPONENT_TEMPLATE" => "top_horizontal_menu",
							"IBLOCK_SERVICES_TYPE" => "catalog",
							"IBLOCK_SERVICES_ID" => "20",
							"IBLOCK_SERVICES_DIR" => SITE_DIR."product/"
						),
						false
					);?>
					<script>
						$(document).ready(function () {
						 $('.adaptiv .top .top_menu .parent .mobile_link').click(function(){
							if ( $(this).parent().hasClass('open') ) {
								$(this).siblings(".submenu_mobile").slideUp();
								$(this).parent().removeClass('open');
							} else {
								$(this).siblings(".submenu_mobile").slideDown();
								$(this).parent().addClass('open');
							}
							return false;
						 });
						});
					</script>
        		</div>
        		<?if($options["POSITION_TOP_MENU"]["ACTIVE_VALUE"] == "catalog"){?>
        			<?$APPLICATION->IncludeComponent("bitrix:menu", "top_catalog", Array(
        				"ROOT_MENU_TYPE" => "catalog",
        				"MENU_CACHE_TYPE" => "A",
        				"MENU_CACHE_TIME" => "36000000",
        				"MENU_CACHE_USE_GROUPS" => "Y",
        				"MENU_CACHE_GET_VARS" => "",
						"CACHE_SELECTED_ITEMS" => "N", // Не создавать кеш меню для каждой страницы
        				"MAX_LEVEL" => "3",
        				"CHILD_MENU_TYPE" => "left",
        				"USE_EXT" => "Y",
        				"DELAY" => "N",
        				"ALLOW_MULTI_SELECT" => "N",
        				"TYPE_MENU" => $options["TYPE_TOP_MENU"]["ACTIVE_VALUE"],
        				"MENU_WIDTH_SIZE" => $options["MENU_WIDTH_SIZE"]["ACTIVE_VALUE"],
        				),
        				false
        			);?>
        		<?}?>
        	</div>
        	<?if($bIsFrontPage || (!$bIsFrontPage && $options["HIDE_MAIN_BANNER"]["ACTIVE_VALUE"] != "Y")){?>
        		<?$APPLICATION->IncludeComponent(
                	"intec:custom.iblock.element.list",
                	"slider.1",
                	array(
                		"COMPONENT_TEMPLATE" => "slider.1",
                		"IBLOCK_TYPE" => "content",
                		"IBLOCK_ID" => "23",
                		"IBLOCK_SECTION" => "",
                		"IBLOCK_SECTION_CODE" => "",
                		"IBLOCK_ELEMENTS_COUNT" => "0",
                		"IBLOCK_ELEMENTS_ID" => array(
                		),
                		"PICTURE_WIDTH" => "0",
                		"USE_DETAIL_PICTURE" => "N",
                		"USE_PREVIEW_PICTURE" => "N",
                		"PICTURE_HEIGHT" => "0",
                		"NO_PICTURE_PATH" => "",
                		"IBLOCK_SORT_FIELD" => "SORT",
                		"IBLOCK_SORT_ORDER" => "asc",
                		"AUTO_SLIDE" => "Y",
                		"AUTO_SLIDE_TIME" => "7500"
                	),
                	false
                );?>
        	<?}?>
        	<div class="clear"></div>
        	<div class="workarea_wrap">
        		<div class="worakarea_wrap_container workarea">
        			<div class="bx_content_section">
        				<?if(!$bIsFrontPage){?>
        					<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "elegante_bread", Array(
        						"START_FROM" => "0",
        						"PATH" => "",
        						"SITE_ID" => SITE_ID,
        						),
        						false
        					);?>
        					<h1 class="header_grey"><?$APPLICATION->ShowTitle("header")?></h1>
        					<?if($bShowMenuTop){?>
        						<div class="top_custom">
        							<?$APPLICATION->IncludeComponent(
        								"bitrix:menu",
        								"custom_menu",
        								array(
        									"ROOT_MENU_TYPE" => "topcustom",
        									"MENU_THEME" => "site",
        									"MENU_CACHE_TYPE" => "N",
        									"MENU_CACHE_TIME" => "3600",
        									"MENU_CACHE_USE_GROUPS" => "Y",
        									"MENU_CACHE_GET_VARS" => array(
        									),
											"CACHE_SELECTED_ITEMS" => "N", // Не создавать кеш меню для каждой страницы
        									"MAX_LEVEL" => "1",
        									"CHILD_MENU_TYPE" => "left",
        									"USE_EXT" => "N",
        									"DELAY" => "N",
        									"ALLOW_MULTI_SELECT" => "N",
        									"COMPONENT_TEMPLATE" => "custom_menu",
        									"HIDE_CATALOG" => "Y",
        									"COUNT_ITEMS_CATALOG" => "8"
        								),
        								false
        							);?>
        						</div>
        					<?}?>
        					<?if($bShowMenuLeft){?>
        						<div class="left_col">
        							<?$APPLICATION->IncludeComponent(
        								"bitrix:menu",
        								"left_menu",
        								array(
        									"ROOT_MENU_TYPE" => "left",
        									"MENU_THEME" => "site",
        									"MENU_CACHE_TYPE" => "N",
        									"MENU_CACHE_TIME" => "3600",
        									"MENU_CACHE_USE_GROUPS" => "Y",
        									"MENU_CACHE_GET_VARS" => array(
        									),
											"CACHE_SELECTED_ITEMS" => "N", // Не создавать кеш меню для каждой страницы
        									"MAX_LEVEL" => "1",
        									"CHILD_MENU_TYPE" => "left",
        									"USE_EXT" => "N",
        									"DELAY" => "N",
        									"ALLOW_MULTI_SELECT" => "N",
        									"COMPONENT_TEMPLATE" => "left_menu",
        									"HIDE_CATALOG" => "Y",
        									"COUNT_ITEMS_CATALOG" => "8"
        								),
        								false
        							);?>
        						</div>
        						<div class="right_col">
        					<?}?>
        				<?}?>
